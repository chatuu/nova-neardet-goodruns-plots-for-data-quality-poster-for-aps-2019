#!/bin/sh

for i in 0 1 2
do

root -l -b -q "PlotEfficiency.C($i)"
root -l -b -q "GoodDataSelDate.C($i)"
root -l -b -q "GoodDataSelRuns.C($i)"
root -l -b -q "GoodDataSelGoodDB.C($i)"
root -l -b -q "GoodDataSelMipRate.C($i)"
root -l -b -q "GoodDataSelNumSlices.C($i)"
root -l -b -q "GoodDataSelTimingPeakStart.C($i)"
root -l -b -q "GoodDataSelTimingPeakEnd.C($i)"
root -l -b -q "GoodDataSelDuration.C($i)"
root -l -b -q "GoodDataSelEmptySpillFrac.C($i)"

done
